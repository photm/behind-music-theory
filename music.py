#!/bin/python3

# sum of squares
def sos(lst):
    return sum(map(lambda x: x ** 2, lst))

def err(tones, quint, comp=1.5):
    return abs(comp - 2 ** (quint/tones))

# "quint" is "fifth" in german.  Actually, fifth is only computed if the default
# comparator of 3 / 2 is used; by altering the comparator using different prime
# numbers, other good-sounding intervals can be found
def quint(tones, comp=1.5):
    return min(range(1,tones+1), key=lambda n: err(tones, n, comp))

def quinterr(tones, comp=1.5):
    return round(err(tones, quint(tones, comp), comp), 5)

def errors(tones):
            # fifth                 # maj third
    return (quinterr(tones, 3 / 2), quinterr(tones, 5 / 4),
            # min third             # sec
            quinterr(tones, 7 / 6), quinterr(tones, 11 / 10),
            # half
            quinterr(tones, 13 / 12))

def tonalities(tones):
    q = quint(tones)
    # |(Z/tones)/q|
    x = q
    res = 1
    while x % tones != 0:
        x += q
        res += 1
    return res


def isprime(n):
    for x in range(2,n-1):
        if n % x == 0:
            return False
    return True
